package gmw.demo.karate.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LocalidadRepository extends JpaRepository<Localidad, Integer> {

    List<Localidad> findByNombreContainingIgnoreCase(String nombre);
}
