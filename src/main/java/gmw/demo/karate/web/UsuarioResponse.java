package gmw.demo.karate.web;


public class UsuarioResponse {

    private Integer id;
    private String nombre;
    private String email;

    public UsuarioResponse() {
    }

    public UsuarioResponse(Integer id, String nombre, String email) {
        this.id = id;
        this.nombre = nombre;
        this.email = email;
    }

    public static UsuarioResponse ejemplo(Integer id) {
        if (id != 100) {
            throw new UsuarioNoExiste("No existe el usuario " + id);
        }
        return new UsuarioResponse(id, "Cosme Fulanito", "cosme@email.com");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
