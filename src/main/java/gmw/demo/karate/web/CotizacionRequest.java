package gmw.demo.karate.web;

import java.math.BigDecimal;

public class CotizacionRequest {
    private BigDecimal precio;
    private Integer cantidad;
    private String producto;
    private String ubicacion;
    private BigDecimal gastosEnvios;

    public CotizacionRequest() {}

    public CotizacionRequest(BigDecimal precio, Integer cantidad, String producto,
                             String ubicacion, BigDecimal gastosEnvios) {
        this.precio = precio;
        this.cantidad = cantidad;
        this.producto = producto;
        this.ubicacion = ubicacion;
        this.gastosEnvios = gastosEnvios;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public BigDecimal getGastosEnvios() {
        return gastosEnvios;
    }

    public void setGastosEnvios(BigDecimal gastosEnvios) {
        this.gastosEnvios = gastosEnvios;
    }
}
