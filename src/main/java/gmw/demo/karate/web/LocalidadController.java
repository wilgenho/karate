package gmw.demo.karate.web;

import gmw.demo.karate.domain.Localidad;
import gmw.demo.karate.domain.LocalidadRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class LocalidadController {

    private LocalidadRepository localidadRepository;

    public LocalidadController(LocalidadRepository localidadRepository) {
        this.localidadRepository = localidadRepository;
    }

    @GetMapping("/localidades")
    public List<Localidad> listar(@RequestParam(name = "q", required = false, defaultValue = "") String busqueda)
    {
        if (busqueda.isEmpty()){
            return localidadRepository.findAll();

        }
        return localidadRepository.findByNombreContainingIgnoreCase(busqueda);
    }
}
