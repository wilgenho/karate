package gmw.demo.karate.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioController {

    @GetMapping("/usuarios/{id}")
    public UsuarioResponse consultar(@PathVariable int id) {
        return UsuarioResponse.ejemplo(id);
    }
}
