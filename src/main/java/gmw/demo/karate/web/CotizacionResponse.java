package gmw.demo.karate.web;

import java.math.BigDecimal;

public class CotizacionResponse {
    private Long numero;
    private BigDecimal importe;
    private String producto;
    private String ubicacion;

    public CotizacionResponse(Long numero, BigDecimal importe, String producto, String ubicacion) {
        this.numero = numero;
        this.importe = importe;
        this.producto = producto;
        this.ubicacion = ubicacion;
    }

    public static CotizacionResponse crearCotizacion(CotizacionRequest pedido, Long numero) {
        BigDecimal cantidad = new BigDecimal(pedido.getCantidad());
        BigDecimal importe = pedido.getPrecio().multiply(cantidad).add(pedido.getGastosEnvios());
        return new CotizacionResponse(numero, importe, pedido.getProducto(), pedido.getUbicacion());
    }

    public Long getNumero() {
        return numero;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }
}
