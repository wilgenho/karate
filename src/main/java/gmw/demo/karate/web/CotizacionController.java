package gmw.demo.karate.web;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CotizacionController {

    @PutMapping("/cotizaciones/{numero}")
    public CotizacionResponse cotizar(@RequestBody CotizacionRequest request, @PathVariable("numero") Long numero) {
        return CotizacionResponse.crearCotizacion(request, numero);
    }
}
