#noinspection CucumberUndefinedStep
Feature: Crear cotizaciones

  Background:
    * url baseUrl

  Scenario: Crear una cotizacion por un Jamon
    Given path 'cotizaciones' , 565
    And request read('cotizacion.json')
    When method put
    Then status 200
    And match response ==
    """
     { numero: 565,
       importe: 1324.87,
       producto: 'Jamon',
       ubicacion: 'Zoom'
     }
    """

  Scenario Outline: Crear cotizaciones con ejemplos
    Given path 'cotizaciones', <nro>
    And request
    """
     {
      "precio": <precio>,
      "cantidad": <cantidad>,
      "producto": '<producto>',
      "ubicacion": "Aqui",
      "gastosEnvios": <gastos>
     }
    """
    When method put
    Then status 200
    And match response contains { numero: <nro>, producto: <producto> }

    Examples:
    | nro | precio  | cantidad | producto   | gastos |
    | 221 | 2312.31 | 23       | Zapatillas | 805    |
    | 308 | 5671.35 | 12       | Camperas   | 905    |

  Scenario: Pedido de cotizacion con error
    Given path 'cotizaciones' , 605
    And request
    """
     {
      "precio": 100.12,
      "cantidad": 'eeee???',
      "producto": "Jamon",
      "ubicacion": "Zoom",
      "gastosEnvios": 123.43
     }
    """
    When method put
    Then status 400
    And match response contains { error: '#string' }


