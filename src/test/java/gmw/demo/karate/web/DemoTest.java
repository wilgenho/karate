package gmw.demo.karate.web;

import com.intuit.karate.junit5.Karate;

import static org.junit.jupiter.api.Assertions.*;

class DemoTest {

    @Karate.Test
    Karate testSample() {
        return Karate.run("sample").relativeTo(getClass());
    }

}