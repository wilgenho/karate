package gmw.demo.karate.web;

import com.intuit.karate.junit5.Karate;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ExtendWith(SpringExtension.class)
class VersionControllerTest {

    @Karate.Test
    Karate deberiaConsultarVersion() {
        return Karate.run("version").relativeTo(getClass());
    }

}