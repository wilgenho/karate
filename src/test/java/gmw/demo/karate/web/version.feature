#noinspection CucumberUndefinedStep
Feature: Servicio para consultar version

  Background:
    * url 'http://localhost:8080'

  Scenario: Consultar version
    Given path '/version'
    When method get
    Then status 200
    And match response == '1.0.1'
    


    