#noinspection CucumberUndefinedStep

Feature: Consulta de usuarios

  Background:
    * url baseUrl

  Scenario: Consultar un usuario existente
    Given path 'usuarios' , 100
    When method get
    Then status 200
    And match response ==
    """
     { id: 100 ,
       nombre: 'Cosme Fulanito',
       email: 'cosme@email.com'
     }
    """

  Scenario: Consultar un usuario que no existe
    Given path 'usuarios' , 999
    When method get
    Then status 404
    And match response contains { error: 'Not Found', message: '#notnull' }