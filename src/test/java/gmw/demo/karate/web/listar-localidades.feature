#noinspection CucumberUndefinedStep
Feature: Listar Localidades

  Background:
    * url baseUrl

  Scenario: Obtener un listado de todas localidades
    Given path 'localidades'
    And header Authorization = 'Bearer xxxxxx'
    And header Accept = 'application/json'
    When method get
    Then status 200
    And match response == '#[2]'
    And match response[0] contains { id: '#number', nombre: 'Tres Arroyos' }
    And match response[*].provincia contains 'Buenos Aires'

  Scenario: Buscar una localidad por nombre
    Given path 'localidades'
    And param q = 'arroyos'
    When method get
    Then status 200
    And match response == '#[1]'
    And match response[0] contains { nombre: 'Tres Arroyos' }

  Scenario: No encontrar localidades
    Given path 'localidades'
    And param q = 'zzzzz'
    When method get
    Then status 200
    And match response == '#[0]'




